all: pdf

pdf:
	# pdflatex -output-directory build control_topology.tex
	pdflatex -output-directory build feedback_explanation.tex
	pdflatex -output-directory build Fig1.tex
	# pdflatex -output-directory build model_2.tex
	# pdflatex -output-directory build model_3.tex
	# pdflatex -output-directory build Fig2.tex
	# pdflatex -output-directory build model_1_osc_lesions.tex
	# pdflatex -output-directory build model_1_cpl_lesions.tex
	# pdflatex -output-directory build model_1_fdb_lesions.tex
	# pdflatex -output-directory build Fig3.tex

clean:
	rm -f *.aux *.log *.nav *.out *.snm *.toc feedback_explanation.pdf control_topology.pdf
